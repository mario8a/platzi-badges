# Platzi Badges

_Proyecto desarrollado en el curso de React de platzi en donde se simula una web page para la platzi conf colombia en donde podemos registrar nuestro badge de la conf. En este proyecto se uso json-serve y faker para tener una simulacion de base de datos para almacenar a los usuarios. Tambien podemos editar y eliminar el badge que creeemos. Se uso react-router para la navegacion_

![Captura de Movie React](./.readme-static/new-badge.jpg)
![Captura de Movie React](./.readme-static/platzi-badges.jpg)

[Ver en netlify](https://heuristic-roentgen-4262ad.netlify.app/)

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._


### Pre-requisitos 📋

_Que cosas necesitas para instalar el software y como instalarlas_

```
Node js
```

### Instalación 🔧

_Clona el repositorio y una vez clonado, instalá las dependencias necesarias ejecutando el siguiente comando:_

```
npm install
```

_Levanta el proyecto con el siguiente comando:_

```
npm start
```

## Construido con 🛠️

_Herramientas utilizadas en el proyecto_

* [React](https://es.reactjs.org/) - El framework web usado
* [React Router](https://reactrouter.com/) - Componentes de navegacion
* [JSON server](https://www.npmjs.com/package/json-server) - fake REST API


---
⌨️ con ❤️ por [Mario Ochoa](https://www.instagram.com/mario_8a_/) 😊
