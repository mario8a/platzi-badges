import React from 'react';
import './styles/PageError.css';

export const PageError = (props) => {
   return (
      <div className="PageError">
         {props.error.message}
      </div>
   )
}
