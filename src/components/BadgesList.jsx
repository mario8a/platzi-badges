import React, { useMemo, useState } from 'react';
import { Link } from 'react-router-dom';
import { Gravatar } from './Gravatar';
import './styles/BadgesList.css';

function useSearchBadges(badges) {
   const [query, setQuery] = useState('');
   //El estado inicial va hacer la lista completa
   const [filteredBadges, setFilteredBadges] = useState(badges);

   useMemo(() => {
      const result = badges.filter(badge => {
      return `${badge.firstName} ${badge.lastName}`
        .toLowerCase()
        .includes(query.toLowerCase());
      });

      setFilteredBadges(result);
   }, [badges, query]);

   return {query, setQuery, filteredBadges}
}

function BadgesList(props) {
   const badges = props.badges;
   // console.log(badges);

   const {query, setQuery, filteredBadges} = useSearchBadges(badges)

   // badgeList = this.props.badges.slice(0).reverse();
      if(filteredBadges.length === 0) {
         return (
            <div>
               <h3>No encontramos ningun badge</h3>
               <div className="form-group mb-4">
                  <label>Filter Badges</label>
                  <input
                     type="text"
                     className="form-control"
                     value={query}
                     onChange={(e) => {
                        setQuery(e.target.value);
                     }}
                  />
               </div>
               <Link className="btn btn-primary" to="/badges/new">
                  Create new Badge
               </Link>
            </div>
         );
      }
      return (
         <div className="BadgesList">
            <div className="form-group mb-4">
               <label>Filter Badges</label>
               <input
                  type="text"
                  className="form-control"
                  value={query}
                  onChange={(e) => {
                     setQuery(e.target.value);
                  }}
               />
            </div>
            <ul className="list-unstyled">
               {filteredBadges.map((badge) => {
                  return (
                     <li key={badge.id}>
                        <Link
                           className="text-reset text-decoration-none"
                           to={`/badges/${badge.id}`}
                        >
                           <div className="BadgesListItem">
                              <Gravatar
                                 className="BadgesListItem__avatar"
                                 email={badge.email}
                                 alt="Avatar"
                              />
                              <div>
                                 <strong>
                                    {badge.firstName} {badge.lastName}
                                 </strong>
                                 <br />@{badge.twitter}
                                 <br />
                                 {badge.jobTitle}
                              </div>
                           </div>
                        </Link>
                     </li>
                  );
               })}
            </ul>
         </div>
      );
}

export default BadgesList;
