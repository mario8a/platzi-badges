import React, { Component } from 'react';

class BadgeForm extends Component {

  handleClick = (e) => {
     console.log('Button');
  }

   render() {
      return (
        <div>

          <form onSubmit={this.props.onSubmit}>
            <div className="form-group">
              <label>First Name</label>
              <input
                onChange={this.props.onChange}
                className="form-control"
                placeholder="introduce tu nombre"
                type="text"
                name="firstName"
                value={this.props.formValues.firstName}
              />
            </div>
            <div className="form-group">
              <label>Last Name</label>
              <input
                onChange={this.props.onChange}
                className="form-control"
                placeholder="introduce tu nombre"
                type="text"
                name="lastName"
                value={this.props.formValues.lastName}
              />
            </div>
            <div className="form-group">
              <label>Email</label>
              <input
                onChange={this.props.onChange}
                className="form-control"
                placeholder="introduce tu nombre"
                type="email"
                name="email"
                value={this.props.formValues.email}
              />
            </div>
            <div className="form-group">
              <label>Job title</label>
              <input
                onChange={this.props.onChange}
                className="form-control"
                placeholder="introduce tu nombre"
                type="text"
                name="jobTitle"
                value={this.props.formValues.jobTitle}
              />
            </div>

            <div className="form-group">
              <label>Twitter</label>
              <input
                onChange={this.props.onChange}
                className="form-control"
                placeholder="introduce tu nombre"
                type="text"
                name="twitter"
                value={this.props.formValues.twitter}
              />
            </div>

            <button onClick={this.handleClick} className="btn btn-primary mt-2">
              Save
            </button>
          </form>

          { this.props.error && ( <p className="text-danger">{this.props.error.message}</p> ) }
        </div>
      );
   }
}

export default BadgeForm;
