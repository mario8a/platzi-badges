import React from "react";
import ReactDOM from "react-dom";
//Componentes
import { App } from "./components/App";
//Styles
import "./global.css";
import "bootstrap/dist/css/bootstrap.css";


const container = document.getElementById("app");

// ReactDOM.render(__qué queremos renderizar__, __dónde renderizar__);
ReactDOM.render(
  <App/>,
  container
);
