import React, { Component } from "react";
import Badge from "../components/Badge";
import BadgeForm from "../components/BadgeForm";
import header from "../images/platziconf-logo.svg";
import "./styles/BadgeNew.css";
import api from '../api';
import { PageLoading } from "../components/PageLoading";

class BadgeNew extends Component {
   state = {
    loading: false,
    error: null,
    form: {
      firstName: "",
      lastName: "",
      email: "",
      jobTitle: "",
      twitter: "",
    },
  };

  handleChange = (e) => {
    this.setState({
      form: {
        ...this.state.form,
        [e.target.name]: e.target.value,
      },
    });
  };

  handleSubmit = async (e) => {
     e.preventDefault();
     this.setState({ loading: true, error: null });
     try {
        await  api.badges.create(this.state.form);
        console.log('Nuevo');
        this.setState({ loading: false });

       this.props.history.push('/badges');

     } catch (error) {
        this.setState({ loading: false, error: error })
     }
  }

  render() {
     if(this.state.loading) {
        return <PageLoading />
     }
    return (
      <div>
        <div className="BadgeNew__hero">
          <img className="BadgeNew__hero-image img-fluid" src={header} alt="" />
        </div>
        <div className="container">
          <div className="row">
            <div className="col-6">
              <Badge
                firstName={this.state.form.firstName || "FIRST_NAME"}
                lastname={this.state.form.lastName || "LAST_NAME"}
                email={this.state.form.email || "@EMAIL"}
                avatarUrl="https://scontent-dfw5-1.xx.fbcdn.net/v/t1.6435-9/55759940_2424394247795616_7375260760575836160_n.jpg?_nc_cat=111&ccb=1-3&_nc_sid=09cbfe&_nc_eui2=AeHKXQqISFnamqYQuedwP9WZbm_wPTid7zBub_A9OJ3vMNjBiliyct4v_UlmilwqoODakgFu8e4jXe4Coz-U1TWC&_nc_ohc=5EjgtIyu74MAX--uuvX&_nc_oc=AQlVloyTDfFRcax5bOZnQINdi7sfrK4xhYnZrdEPkxzMJNJBD8be_nwObuYpUnVBhmg&_nc_ht=scontent-dfw5-1.xx&oh=db2fcb29b54a0d1de5f6a7cd66616f28&oe=60E6AA1E"
                jobTitle={this.state.form.jobTitle || "JOB_TITLE"}
                twitter={this.state.form.twitter || "twitter"}
              />
            </div>
            <div className="col-6">
            <h1>New attendant</h1>
              <BadgeForm
                onChange={this.handleChange}
                onSubmit={this.handleSubmit}
                formValues={this.state.form}
                error={this.state.error}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default BadgeNew;
