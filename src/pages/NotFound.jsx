import React from 'react'
import notFound from '../images/404.svg';
import './styles/NotFound.css'

export const NotFound = () => {
   return (
      <div className="not-found">
         <img src={notFound} alt="Not found" width="300"/>
      </div>
   )
}
